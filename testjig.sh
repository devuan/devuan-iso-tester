#!/bin/bash
#
# This script sets up a "testjig" for testing a given ISO ($1) for a
# given emulation ($2) and run through a given test scenario ($3).
#
# optional recording/viewing.
# use env HEADLESS=y to run without
# use env HEADLESS=record to only record
# use HEADLESS="" for viewing
#
# Starts a virtual X on display 101.0 wit default screen which is
# 1280x1024 (24 bit colour).
#
# Runs qemu in foreground with multiplexed serial console and qemu
# monitor, where keys ^\c alternates between them.
#
# It also reports test script run in foreground (stdout).

PKG="xvfb x11-apps xdotool ffmpeg qemu-system"
dpkg -l $PKG >& /dev/null || apt install $PKG

ISO=$1
EMUL=${2%-*}
EMULB=${2#*-}
TEST=$3

JIG=:101
Xvfb $JIG &
PIDS="$!"
sleep 1
terminate() {
    kill $PIDS
    trap "" 0
}
trap "terminate" 0 2 15

# start recording and viewing
FFMPEG="-f x11grab -video_size 1280x1024 -framerate 25 -i $JIG.0 -f mpegts"
if [ -z "$HEADLESS" ] ; then
   FFPLAY="-fflags nobuffer -flags low_delay -x 960 -y 768 -autoexit -f mpegts"
   ( ffmpeg $FFMPEG - | ffplay $FFPLAY -i - ) 2>/dev/null &
   for i in {1..10} ; do echo -n .; sleep 1 ; done ; echo
elif [ "$HEADLESS" = "record" ] ; then
    ( ffmpeg $FFMPEG - | ffplay $FFPLAY -i $TEST-capture.ts ) 2>/dev/null &
    sleep 2
else
    sleep 2   
fi

case "$EMUL" in
    5|6) export USBHOST=$EMULB ;;
    11|12) export SCSIHOST=$EMULB ;;
esac

export FILE=$ISO
export ISOTYPE=amd64

scripting() {
    sleep 5;
    SNAP=1
    while read LINE ; do
	echo "$TEST: $LINE"
	case "$LINE" in
	    sleep*) $LINE ;;
	    snap*)
		SHA="$(xwd -name QEMU | tee $TEST-snap.$SNAP | shasum)"
		echo "$TEST:  got ${SHA%% *}"
		[ "$LINE" != "snap ${SHA%% *}" ] && ERR+=" $SNAP"
		SNAP=$((SNAP+1))
		;;
	    key*|type*|mousemove*) xdotool $LINE ; sleep 1 ;;
	esac
    done
    echo "ERR:$ERR" > $TEST.result
    kill $1
}
if [ -f "$TEST" ] ; then
    DISPLAY=$JIG.0 scripting $$ < $TEST &
fi

DISPLAY=$JIG.0 ./vm.sh $EMUL
wait
