Devuan ISO Tester
=================

This is a project to run tests of Devuan Installer ISO's as a final step of the build phase - booting in virtual environments to catch potential issues before publication.

Based on initial concept of vm.sh by rrq.
